import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NavItemComponent } from './components/nav-item/nav-item.component';
import { StoreComponent } from './components/store/store.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { CreateStoreComponent } from './components/store/create-store/create-store.component';
import { ActionsComponent } from './components/actions/actions.component';
import { AngularFirestoreModule} from '@angular/fire/compat/firestore';
import { AngularFireModule } from '@angular/fire/compat';
import { CategoryComponent } from './components/category/category.component';
import { CreateEmployeeComponent } from './components/employee/create-employee/create-employee.component';
import { CreateCategoryComponent } from './components/category/create-category/create-category.component';
import { EditStoreComponent } from './components/store/edit-store/edit-store.component';
import { EditCategoryComponent } from './components/category/edit-category/edit-category.component';
import { VoucherComponent } from './components/voucher/voucher.component';
import { CreateVoucherComponent } from './components/voucher/create-voucher/create-voucher.component';
import { EditVoucherComponent } from './components/voucher/edit-voucher/edit-voucher.component';
import { LoginComponent } from './components/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    NavItemComponent,
    StoreComponent,
    EmployeeComponent,
    CreateStoreComponent,
    ActionsComponent,
    CategoryComponent,
    CreateEmployeeComponent,
    CreateCategoryComponent,
    EditStoreComponent,
    EditCategoryComponent,
    VoucherComponent,
    CreateVoucherComponent,
    EditVoucherComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTabsModule,
    MatIconModule,
    MatDividerModule,
    MatSidenavModule,
    MatToolbarModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyCf50GKg4O1Cob01U56mOVTBPCzV-oPR04",
      authDomain: "cafeteria-ef616.firebaseapp.com",
      projectId: "cafeteria-ef616",
      storageBucket: "cafeteria-ef616.appspot.com",
      messagingSenderId: "777787963195",
      appId: "1:777787963195:web:4629c014d1b216f50d691e",
      measurementId: "G-0W4QXEMNYZ"
    }),
    AngularFirestoreModule,
  ],
  providers: [ActionsComponent, StoreComponent, EmployeeComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
