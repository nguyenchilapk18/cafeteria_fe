import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StoreComponent } from './components/store/store.component';
import { EditStoreComponent } from './components/store/edit-store/edit-store.component';
import { CreateStoreComponent } from './components/store/create-store/create-store.component';

import { EmployeeComponent } from './components/employee/employee.component';
import { CreateEmployeeComponent } from './components/employee/create-employee/create-employee.component';

import { CreateCategoryComponent } from './components/category/create-category/create-category.component';
import { CategoryComponent } from './components/category/category.component';
import { EditCategoryComponent } from './components/category/edit-category/edit-category.component';

import { VoucherComponent } from './components/voucher/voucher.component';
import { CreateVoucherComponent } from './components/voucher/create-voucher/create-voucher.component';
import { EditVoucherComponent } from './components/voucher/edit-voucher/edit-voucher.component';

import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  {path: 'store', component: StoreComponent},
  {path: 'store/create', component: CreateStoreComponent},
  {path: 'store/edit/:storeId', component: EditStoreComponent},

  {path: 'employee', component: EmployeeComponent},
  {path: 'employee/create', component: CreateEmployeeComponent},

  {path: 'category', component: CategoryComponent},
  {path: 'category/create', component: CreateCategoryComponent},
  {path: 'category/edit/:categoryId', component: EditCategoryComponent},

  {path: 'voucher', component: VoucherComponent},
  {path: 'voucher/create', component: CreateVoucherComponent},
  {path: 'voucher/edit/:voucherId', component: EditVoucherComponent},

  {path: 'login', component: LoginComponent},

  {path: '', redirectTo: '/store', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
