import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable,of} from 'rxjs';
import { Employee } from '../components/employee/employee.component';
import { BehaviorSubject } from 'rxjs';
import { map } from 'jquery';
import { EmployeeComponent } from './../components/employee/employee.component';

const httpOptions ={
  headers:new HttpHeaders({'Content-Type':'Application/json'})
}
const apiUrl = 'http://localhost:8080/api/v1/login';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _isLoggedIn = new BehaviorSubject<Boolean>(false);
  isLoggedIn = this._isLoggedIn.asObservable();

  constructor(private httpClient:HttpClient) {
    const token = localStorage.getItem('cafeteria_auth');
    this._isLoggedIn.next(!!token);
  }

  login(employee: Employee):Observable<Employee> {
    this._isLoggedIn.next(true);
    return this.httpClient.post<Employee>(apiUrl, employee).pipe();
  }
}
