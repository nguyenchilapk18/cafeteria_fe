import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable,of} from 'rxjs';
import {Store} from './store.component';

const httpOptions ={
  headers:new HttpHeaders({'Content-Type':'Application/json'})
}
const apiUrl = 'http://localhost:8080/api/v1/store';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor(private httpClient:HttpClient) { }

  getAllStore():Observable<Store[]>{
    return this.httpClient.get<Store[]>(apiUrl + '/all').pipe();
  }

  getStoreById(storeId: Number):Observable<Store>{
    return this.httpClient.get<Store>(apiUrl + "/" + storeId).pipe();
  }

  createStore(store: Store):Observable<any>{
    return this.httpClient.post<any>(apiUrl, store).pipe();
  }

  deleteOneStore(storeId: Number):Observable<Number>{
    return this.httpClient.delete<Number>(apiUrl + "/" + storeId).pipe();
  }

  editStore(store: Store):Observable<any>{
    return this.httpClient.put<Store>(apiUrl, store).pipe();
  }
}
