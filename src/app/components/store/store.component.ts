import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ActionsComponent } from '../actions/actions.component';
import { StoreService } from './store.service';
import { Employee } from '../employee/employee.component';
import { Category } from '../category/category.component';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css'] ,
})
export class StoreComponent implements OnInit {

  actionCreateText = "Create new store";
  actionCreateLink = "/store/create";

  storeList: Array<Store> = [];

  constructor(private storeService: StoreService, private actionComponent: ActionsComponent, private router: Router) {
    
  }

  ngOnInit(): void {
    this.getAll();
  }
  //CheckBox All Handler
  checkBoxAllHandler(checkBoxAllIsSelected:Boolean) {
    this.storeList.forEach(store => {
      store.isSelected = checkBoxAllIsSelected;
    });
  }

  //CheckBox Handler Change
  onCheckBoxChange() {
    this.actionComponent.checkBoxAllClicked(this.storeList.every(item => {
      return item.isSelected == true;
    }));
  }

  //Pagination handler
  reRenderStoreList(storeList:Array<Store>) {
    this.storeList = storeList;
  }

  onClickBtnDelete(store:Store) {
    if(confirm('Are you sure to delete this store ?!')) {
      this.deleteOneStore(store);
      this.storeList.splice(this.storeList.indexOf(store), 1);
    }
    else return;
  }

  //-----------------------------------------------
  //---------------Restful API---------------------
  //-----------------------------------------------
  getStoreListPromise() {
    return this.storeService.getAllStore();
  }

  getAll() {
    return this.storeService.getAllStore().subscribe((res:any) => {
      this.storeList = res;
    }, (error:any) => {

    })
  }

  deleteOneStore(store: Store) {
    this.storeService.deleteOneStore(store.storeId).subscribe((res:any) => {
      if (res) alert(`${store.storeName} with ID ${store.storeId} deleted successfully !!!`);
      else alert(`Could not find this store with ID ${store.storeId} !!!`);
    })
  }

  deleteStore() {
    if(confirm('Are you sure to delete these stores ?!')) {
      const storeCheckedList = this.storeList.filter(function(store:Store){
        return store.isSelected;
      });
      for(let i = 0; i < storeCheckedList.length; i++) {
        this.deleteOneStore(storeCheckedList[i]);
        this.storeList.splice(this.storeList.indexOf(storeCheckedList[i]), 1);
      }
      this.router.navigate(['store']);
    }
    else return;
  }
  

}

export class Store {
    storeId: number;
    storeName: String;
    storeAddress: String;
    storeImage: String;
    employeeList: Array<Employee>[];
    categoryList: Array<Category>[];
    isSelected: Boolean;
    constructor(){
      this.storeId = 0;
      this.storeName = 'Store';
      this.storeAddress = 'Address';
      this.storeImage = 'Image';
      this.employeeList = [];
      this.categoryList = [];
      this.isSelected = false;
    }
}
