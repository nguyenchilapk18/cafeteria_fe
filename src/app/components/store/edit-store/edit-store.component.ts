import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Store } from '../store.component';
import { StoreService } from '../store.service';

@Component({
  selector: 'app-edit-store',
  templateUrl: './edit-store.component.html',
  styleUrls: ['./edit-store.component.css']
})
export class EditStoreComponent implements OnInit {

  selectedFile: any;
  store: Store = new Store();
  imageSrc: String = "";

  constructor(private af: AngularFireStorage, private router: Router, private route: ActivatedRoute, private storeService: StoreService) {
    this.getCurrentStore();
  }

  ngOnInit(): void {
   
  }

  getCurrentStore() {
    const storeId = Number.parseInt(this.route.snapshot.paramMap.get('storeId')!);
    this.storeService.getStoreById(storeId).subscribe(store => {
      this.store = store;
      this.imageSrc = store.storeImage;
    });
  }

  onFileSelected(event:any) {
    this.selectedFile = event.target.files[0];
    if (this.selectedFile) {
      const reader = new FileReader();
      reader.onload = (e:any) => {
        if (typeof reader.result == 'string') {
          this.imageSrc = reader.result;
        }
      } 
      reader.readAsDataURL(this.selectedFile);
    }
  }

  UploadImageAndEditStore(){
    if (this.selectedFile){
      const path = `stores/${this.selectedFile.name}-${Math.round(Math.random()*1000000)}`;
      const ref = this.af.ref(path);
      this.af.upload(path, this.selectedFile).snapshotChanges().pipe(
        finalize( () => {
          ref.getDownloadURL().subscribe(url => {
            this.store.storeImage = url; 
            this.editStore();
          })
        })
      ).subscribe();
    }
  }

  editStore() {
    this.storeService.editStore(this.store).subscribe(res => {
      alert(`Edit store ${this.store.storeId} successfully !!!`);
      this.router.navigate(['store']);
    }, error => {
      alert(`Edit store ${this.store.storeId} failed !! Error: ` + error);
    });
  }

  onSubmitEditStore(event:any) {
    if (this.selectedFile) {
      this.UploadImageAndEditStore();
    }
    else this.editStore();
  }

}
