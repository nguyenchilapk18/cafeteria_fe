import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { Router } from '@angular/router';
import * as $ from 'jquery';

import { finalize } from 'rxjs';
import { Category, Beverage } from '../category.component';
import { Store } from '../../store/store.component';

import { StoreService } from '../../store/store.service';
import { CategoryService } from '../category.service';
import { BeverageService } from '../beverage.service';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.css']
})
export class CreateCategoryComponent implements OnInit {

  category = new Category();

  imageSrc: String;
  selectedFile: any ;
  sizeList: Array<Beverage> = [new Beverage(), new Beverage(), new Beverage()];
  storeList: Array<Store> = [];

  constructor(private af: AngularFireStorage, private router: Router , 
              private categoryService: CategoryService, private beverageService: BeverageService, private storeService: StoreService) {
    this.category.categoryImage =  "/assets/image/default-category.jpg";
    this.imageSrc = this.category.categoryImage;
    this.getAllStore();
  }

  ngOnInit(): void {
  }

  onFileSelected(event:any) {
    this.selectedFile = event.target.files[0];
    if (this.selectedFile) {
      const reader = new FileReader();
      reader.onload = (e:any) => {
        if (typeof reader.result == 'string') {
          this.imageSrc = reader.result;
        }
      } 
      reader.readAsDataURL(this.selectedFile);
    }
  }

  onSelectStoreChange(event:any) {
    this.category.storeId = event.target.value;
  }

  createBeverage(size: Beverage, beverage: Category) {
    let sizeName:String;
    this.beverageService.createBeverage(size).subscribe(res => {
      if (size.beverageSize == 1) sizeName = 'S';
      if (size.beverageSize == 2) sizeName = 'M';
      if (size.beverageSize == 3) sizeName = 'L';
      if (res) alert(`Create beverage ${beverage.categoryName} with size ${sizeName} successfully !!!`)
      else alert(`Could not create beverage ${beverage.categoryName} with size ${sizeName}.`);
    })
  }

  createCategory() {
    this.categoryService.createCategory(this.category).subscribe(res => {
      for (let i = 0; i < this.sizeList.length; i++) {
        this.sizeList[i].beverageSize = i + 1;
        this.sizeList[i].categoryId = res.categoryId;
        this.createBeverage(this.sizeList[i], res);
      }
    })
  }

  UploadImageAndCreateCategory(){
    if (this.selectedFile){
      const path = `categories/${this.selectedFile.name}-${Math.round(Math.random()*100000000)}`;
      const ref = this.af.ref(path);
      this.af.upload(path, this.selectedFile).snapshotChanges().pipe(
        finalize( () => {
          ref.getDownloadURL().subscribe(url => {
            this.category.categoryImage = url; 
            this.createCategory();
          })
        })
      ).subscribe();
    }
  }

  onSubmitCreateCategory(event:any) {
    if (this.selectedFile) {
      this.UploadImageAndCreateCategory();
    }
    else this.createCategory();
  }

  getAllStore() {
    return this.storeService.getAllStore().subscribe((res:any) => {
      this.storeList = res;
    }, (error:any) => {

    })
  }
}
