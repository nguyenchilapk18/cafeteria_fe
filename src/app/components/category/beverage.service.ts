import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable,of} from 'rxjs';
import { Category, Beverage } from './category.component';

const httpOptions = {
  headers:new HttpHeaders({'Content-Type':'Application/json'})
}
const apiUrl = 'http://localhost:8080/api/v1/beverage';

@Injectable({
  providedIn: 'root'
})
export class BeverageService {

  constructor(private httpClient:HttpClient) { }

  getAllBeverage():Observable<Beverage[]> {
    return this.httpClient.get<Beverage[]>(apiUrl + "/all").pipe();
  }

  getAllBeverageByCategoryId(categoryId: Number):Observable<Beverage[]> {
    if (categoryId == 0) return this.getAllBeverage();
    return this.httpClient.get<Beverage[]>(apiUrl + "/category/" + categoryId).pipe();
  }

  createBeverage(beverage: Beverage):Observable<Beverage> {
    return this.httpClient.post<Beverage>(apiUrl, beverage).pipe();
  }

  deleteOneBeverage(beverageId: Number):Observable<Number> {
    return this.httpClient.delete<Number>(apiUrl + "/" + beverageId).pipe();
  }
}
