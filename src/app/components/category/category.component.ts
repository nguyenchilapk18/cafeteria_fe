import { Component, OnInit } from '@angular/core';
import { ActionsComponent } from '../actions/actions.component';
import { CategoryService } from './category.service';
import { Store } from './../store/store.component';
import { BeverageService } from './beverage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  actionCreateText = "Create new beverage";
  actionCreateLink = "/category/create";

  selectedStore: Number = 0;
  storeList: Array<Store> = [];
  categoryList: Array<Category> = [];

  constructor(private actionComponent: ActionsComponent, private categoryService: CategoryService, private beverageService: BeverageService, 
            private router:Router) { }

  ngOnInit(): void {
    this.getAllCategory();
  }

  //CheckBox All Handler
  checkBoxAllHandler(checkBoxAllIsSelected:Boolean) {
    this.categoryList.forEach(category => {
      category.isSelected = checkBoxAllIsSelected;
    });
  }

  //CheckBox Handler Change
  onCheckBoxChange() {
    this.actionComponent.checkBoxAllClicked(this.categoryList.every(item => {
      return item.isSelected == true;
    }));
  }

  storeSelected(storeId: Number) {
    this.selectedStore = storeId;
  }

  reRenderCategoryList(categoryList: Array<Category>) {
    this.categoryList = categoryList;
  }

  onClickBtnDelete(category:Category){
    if(confirm('Are you sure to delete this category ?!')) {
      this.deleteOneCategory(category);
    }
    else return;
  }  
  //-----------------------------------------------
  //---------------Restful API---------------------
  //-----------------------------------------------
  getCategoryListPromise() {
    return this.categoryService.getAllCategoryByStoreId(this.selectedStore);
  } 

  getAllCategory() {
    return this.categoryService.getAllCategoryByStoreId(this.selectedStore).subscribe((res:any) => {
      this.categoryList = res;
      for (let i = 0; i < this.categoryList.length; i++) {
        this.beverageService.getAllBeverageByCategoryId(this.categoryList[i].categoryId).subscribe(res => {
          if (res) {
            this.categoryList[i].beverageList = res;
          }
        })
      }
    }, (error:any) => {
  
    })
  }

  deleteOneCategory(category: Category) {
    this.categoryService.deleteOneCategory(category.categoryId).subscribe((res:any) => {
      if (res) alert(`Beverage ${category.categoryName} with ID ${category.categoryId} deleted successfully !!!`);
      else alert(`Could not find employee with ID ${category.categoryId} !!!`);
    })
  }

  deleteCategory() {
    if(confirm('Are you sure to delete these categories ?!')) {
      const categoryCheckedList = this.categoryList.filter(category => category.isSelected);
      for(let i = 0; i < categoryCheckedList.length; i++) {
        this.deleteOneCategory(categoryCheckedList[i]);
        this.categoryList.splice(this.categoryList.indexOf(categoryCheckedList[i]), 1);
      }
      this.router.navigate(['category']);
    }
    else return;
  }
}

export class Category {
  categoryId: number;
  categoryName: String;
  categoryImage: String;
  storeId: number;
  beverageList: Array<Beverage>;
  isSelected: Boolean;
  constructor(){
    this.storeId = 1;
    this.categoryId = 0;
    this.categoryName = '';
    this.categoryImage = '/assets/image/default-category.jpg';
    this.isSelected = false;
    this.beverageList = [];
  }
}

export class Beverage {
  beverageId: number;
  beverageSize: Number;
  beveragePrice: Number;
  categoryId: number;
  constructor() {
    this.beverageId = 0;
    this.beveragePrice = 0;
    this.beverageSize = 1;
    this.categoryId = 1;
  } 
}
