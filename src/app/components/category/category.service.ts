import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable,of} from 'rxjs';
import { Category } from './category.component';

const httpOptions = {
  headers:new HttpHeaders({'Content-Type':'Application/json'})
}
const apiUrl = 'http://localhost:8080/api/v1/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private httpClient:HttpClient) {
    
  }

  getCategoryById(categoryId: Number):Observable<Category> {
    return this.httpClient.get<Category>(apiUrl + "/" + categoryId).pipe();
  }

  getAllCategory():Observable<Category[]> {
    return this.httpClient.get<Category[]>(apiUrl + "/all").pipe();
  }

  getAllCategoryByStoreId(storeId: Number):Observable<Category[]> {
    if (storeId == 0) return this.getAllCategory();
    return this.httpClient.get<Category[]>(apiUrl + "/store/" + storeId).pipe();
  }

  createCategory(category: Category):Observable<Category> {
    return this.httpClient.post<Category>(apiUrl, category).pipe();
  }

  deleteOneCategory(categoryId: Number):Observable<Number> {
    return this.httpClient.delete<Number>(apiUrl + "/" + categoryId).pipe();
  }
  
}
