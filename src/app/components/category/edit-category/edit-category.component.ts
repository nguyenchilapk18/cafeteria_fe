import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs';

import { Category, Beverage } from '../category.component';
import { CategoryService } from '../category.service';
import { BeverageService } from '../beverage.service';

import { Store } from '../../store/store.component';
import { StoreService } from '../../store/store.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.css']
})
export class EditCategoryComponent implements OnInit {

  category: Category = new Category();
  sizeList: Array<Beverage> = [new Beverage(), new Beverage(), new Beverage()];
  storeList: Array<Store> = [];
  imageSrc: String = "";
  selectedStore: Number = 0;
  selectedFile: any;

  constructor(private af: AngularFireStorage, private storeService: StoreService, private route: ActivatedRoute, private categoryService: CategoryService) {

  }

  ngOnInit(): void {
    this.getAllStore();
    this.getCurrentCategory();
  }

  getAllStore() {
    return this.storeService.getAllStore().subscribe((res:any) => {
      this.storeList = res;
    }, (error:any) => {

    })
  }

  getCurrentCategory() {
    const categoryId =  Number.parseInt(this.route.snapshot.paramMap.get('categoryId')!);
    this.categoryService.getCategoryById(categoryId).subscribe(cat => {
      this.category = cat;
      this.imageSrc = this.category.categoryImage;
      this.category.beverageList.forEach((beverage, index) => {
        this.sizeList[index] = beverage;
      });
    });
  }

  onSelectStoreChange(event:any) {
    this.selectedStore = event.target.value;    
  }

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
    if (this.selectedFile) {
      const reader = new FileReader();
      reader.onload = (e:any) => {
        if (typeof reader.result == 'string') {
          this.imageSrc = reader.result;
        }
      } 
      reader.readAsDataURL(this.selectedFile);
    }
  }

  UploadImageAndEditCategory(){
    if (this.selectedFile){
      const path = `categories/${this.selectedFile.name}-${Math.round(Math.random()*1000000)}`;
      const ref = this.af.ref(path);
      this.af.upload(path, this.selectedFile).snapshotChanges().pipe(
        finalize( () => {
          ref.getDownloadURL().subscribe(url => {
            this.category.categoryImage = url; 
            this.editCategory();
          })
        })
      ).subscribe();
    }
  }

  editCategory() {
    
  }

  onSubmitEditCategory(event:any) {

  }
  
} 
