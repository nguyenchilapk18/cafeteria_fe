import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  @ViewChild('btnOpenModal') btnOpenModal!: ElementRef;
  @Input() modalTitle: String = "Notification"; 

  constructor() { 

  }

  ngOnInit(): void {
  }

  showModal() {
    this.btnOpenModal.nativeElement.click();
  }

}
