import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-nav-item',
  templateUrl: './nav-item.component.html',
  styleUrls: ['./nav-item.component.css']
})
export class NavItemComponent implements OnInit {

  navConcept =  window.location.href.split('/')[3];
  navList: Array<navItem> = [];

  constructor() {
    this.navList.push(
      {navIcon: 'store', navTitle: 'Stores', navRouterLink: 'store'},
      {navIcon: 'supervisor_account', navTitle: 'Employees', navRouterLink: 'employee'},
      {navIcon: 'local_cafe', navTitle: 'Beverages', navRouterLink: 'category'},
      {navIcon: 'card_giftcard', navTitle: 'Vouchers', navRouterLink: 'voucher'},
      {navIcon: 'receipt', navTitle: 'Orders', navRouterLink: 'order'},
      {navIcon: 'settings', navTitle: 'Settings', navRouterLink: 'setting'},
    )
  }

  ngOnInit(): void {
  }

}

class navItem {
    navIcon: String;
    navTitle: String;
    navRouterLink: String;
    constructor () {
        this.navIcon = "home";
        this.navTitle = "Home";
        this.navRouterLink = "id";
    }
}