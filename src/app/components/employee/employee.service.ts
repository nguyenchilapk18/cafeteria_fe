import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable,of} from 'rxjs';
import {Employee} from './employee.component';

const httpOptions ={
  headers:new HttpHeaders({'Content-Type':'Application/json'})
}
const apiUrl = 'http://localhost:8080/api/v1/employee';

@Injectable({
  providedIn: 'root'
})

export class EmployeeService {

  constructor(private httpClient:HttpClient) { }

  getAllEmployee():Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(apiUrl + "/all").pipe();
  }

  getAllEmployeeByStoreId(storeId: Number):Observable<Employee[]> {
    if (storeId == 0) return this.getAllEmployee();
    return this.httpClient.get<Employee[]>(apiUrl + "/store/" + storeId).pipe();
  }

  createEmployee(employee: Employee):Observable<Employee> {
    return this.httpClient.post<Employee>(apiUrl, employee).pipe();
  }

  deleteOneEmployee(employeeId: Number):Observable<Number> {
    return this.httpClient.delete<Number>(apiUrl + "/" + employeeId).pipe();
  }
}
