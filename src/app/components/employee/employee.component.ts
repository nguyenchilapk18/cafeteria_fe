import { Component, OnInit } from '@angular/core';
import { ActionsComponent } from '../actions/actions.component';
import { EmployeeService } from './employee.service';
import { Store } from './../store/store.component';
import { StoreService } from './../store/store.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  actionCreateText = "Create new employee";
  actionCreateLink = "/employee/create";

  selectedStore: Number = 0;
  storeList: Array<Store> = [];
  employeeList: Array<Employee> = [];

  constructor(private employeeService: EmployeeService, private actionComponent: ActionsComponent, private router: Router) {
    
  }

  ngOnInit(): void {
    this.getAll();
  }

  //CheckBox All Handler
  checkBoxAllHandler(checkBoxAllIsSelected:Boolean) {
    this.employeeList.forEach(emlpoyee => {
      emlpoyee.employeeIsSelected = checkBoxAllIsSelected;
    });
  }

  //CheckBox Handler Change
  onCheckBoxChange() {
    this.actionComponent.checkBoxAllClicked(this.employeeList.every(item => {
      return item.employeeIsSelected == true;
    }));
  }

  storeSelected(storeId: Number) {
    this.selectedStore = storeId;
  }

  reRenderEmployeeList(employeeList: Array<Employee>) {
    this.employeeList = employeeList;
  }

  onClickBtnDelete(employee:Employee) {
    if(confirm('Are you sure to delete this employee ?!')) {
      this.deleteOneEmployee(employee);
      this.employeeList.splice(this.employeeList.indexOf(employee), 1);
    }
    else return;
  }

  //-----------------------------------------------
  //---------------Restful API---------------------
  //-----------------------------------------------

  getEmployeeListPromise() {
    return this.employeeService.getAllEmployeeByStoreId(this.selectedStore);
  }

  getAll() {
    return this.employeeService.getAllEmployeeByStoreId(this.selectedStore).subscribe((res:any) => {
      this.employeeList = res;
    }, (error:any) => {

    })
  }

  deleteOneEmployee(employee: Employee) {
    this.employeeService.deleteOneEmployee(employee.employeeId).subscribe((res:any) => {
      if (res) alert(`Employee ${employee.employeeName} with ID ${employee.employeeId} deleted successfully !!!`);
      else alert(`Could not find employee with ID ${employee.employeeId} !!!`);
    })
  }

  deleteEmployee() {
    if(confirm('Are you sure to delete these employees ?!')) {
      const employeeCheckedList = this.employeeList.filter(function(employee:Employee){
        return employee.employeeIsSelected;
      });
      for(let i = 0; i < employeeCheckedList.length; i++) {
        this.deleteOneEmployee(employeeCheckedList[i]);
        this.employeeList.splice(this.employeeList.indexOf(employeeCheckedList[i]), 1);
      }
      this.router.navigate(['employee']);
    }
    else return;
  }

}

export class Employee {
  employeeId: Number;
  employeeName: String;
  employeeEmail: String;
  employeePhone: String;
  employeeGender: Number;
  employeeRole: Number;
  employeeImage: String;
  employeeLoginName: String;
  employeePassword: String;
  employeeIsSelected: Boolean;
  storeId: Number;
  jwt!: string;
  constructor() {
    this.employeeId = 0;
    this.employeeName = "";
    this.employeeEmail = "";
    this.employeePhone = "";
    this.employeeGender = 0;
    this.employeeRole = 0;
    this.employeeImage = "/assets/image/default-employee.jpg";
    this.employeeLoginName = "";
    this.employeePassword = "";
    this.employeeIsSelected = false;
    this.storeId = 1;
  }
}
