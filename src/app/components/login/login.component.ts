import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Employee } from '../employee/employee.component';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  employee: Employee = new Employee();

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  onLoginSubmit() {
    this.authService.login(this.employee).subscribe(res => {
      localStorage.setItem('cafeteria_auth', res.jwt);
    })
  }

}
