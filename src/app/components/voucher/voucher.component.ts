import { Component, OnInit } from '@angular/core';
import { ActionsComponent } from '../actions/actions.component';
import { VoucherService } from './voucher.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-voucher',
  templateUrl: './voucher.component.html',
  styleUrls: ['./voucher.component.css']
})
export class VoucherComponent implements OnInit {

  actionCreateText: String = 'Create new voucher' ;
  actionCreateLink: String = '/voucher/create';

  voucherList: Array<Voucher> = [];

  constructor(private actionComponent: ActionsComponent, private voucherService: VoucherService, private router: Router) { }

  ngOnInit(): void {
    this.getAllVoucher();

  }

  //CheckBox All Handler
  checkBoxAllHandler(checkBoxAllIsSelected:Boolean) {
    this.voucherList.forEach(voucher => {
      voucher.isSelected = checkBoxAllIsSelected;
    });
  }

  //CheckBox Handler Change
  onCheckBoxChange() {
    this.actionComponent.checkBoxAllClicked(this.voucherList.every(item => {
      return item.isSelected == true;
    }));
  }

  onClickBtnDelete(voucher: Voucher) {
    if(confirm('Are you sure to delete this voucher ?!')) {
      this.deleteOneVoucher(voucher);
      this.voucherList.splice(this.voucherList.indexOf(voucher), 1);
    }
    else return;
  }

  reRenderVoucherList(voucherList:Array<Voucher>) {
    this.voucherList = voucherList;
  }

  //-----------------------------------------------
  //---------------Restful API---------------------
  //-----------------------------------------------
  getVoucherListPromise() {
    return this.voucherService.getAllVoucher();
  }

  getAllVoucher() {
    this.voucherService.getAllVoucher().subscribe(res => {
      this.voucherList = res;
    }) 
  }

  deleteOneVoucher(voucher: Voucher) {
    this.voucherService.deleteOneVoucher(voucher.voucherId).subscribe((res:any) => {
      if (res) alert(`Voucher ${voucher.voucherCode} deleted successfully !!!`);
      else alert(`Could not find voucher ${voucher.voucherCode} !!!`);
    })
  }

  deleteVoucher() {
    if(confirm('Are you sure to delete these vouchers ?!')) {
      const voucherCheckedList = this.voucherList.filter(function(voucher:Voucher){
        return voucher.isSelected;
      });
      for(let i = 0; i < voucherCheckedList.length; i++) {
        this.deleteOneVoucher(voucherCheckedList[i]);
        this.voucherList.splice(this.voucherList.indexOf(voucherCheckedList[i]), 1);
      }
      this.router.navigate(['voucher']);
    }
    else return;
  }
}

export class Voucher {
  voucherId: number;
  voucherCode: String;
  voucherStartDate: Date;
  voucherEndDate: Date;
  voucherPercentage: number;
  voucherMax: Number;
  voucherMinOrder: Number;
  voucherLimit: Number;
  constructor() {
    this.voucherId = 0;
    this.voucherCode = '';
    this.voucherStartDate = new Date(Date.now());
    this.voucherEndDate = new Date(Date.now());
    this.voucherPercentage = 0.1;
    this.voucherMax = 10000;
    this.voucherMinOrder = 50000;
    this.voucherLimit = 100;
    this.isSelected = false;
  }

  isSelected: Boolean;
}