import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable,of} from 'rxjs';
import { Voucher } from './voucher.component';

const httpOptions ={
  headers:new HttpHeaders({'Content-Type':'Application/json'})
}
const apiUrl = 'http://localhost:8080/api/v1/voucher';

@Injectable({
  providedIn: 'root'
})
export class VoucherService {

  constructor(private httpClient:HttpClient) { }

  getVoucherById(voucherId: Number) {
    return this.httpClient.get<Voucher>(apiUrl + "/" + voucherId).pipe();
  }

  getAllVoucher():Observable<Voucher[]> {
    return this.httpClient.get<Voucher[]>(apiUrl + "/all").pipe();
  }

  createVoucher(voucher: Voucher):Observable<Voucher> {
    return this.httpClient.post<Voucher>(apiUrl, voucher).pipe();
  }

  deleteOneVoucher(voucherId: Number):Observable<Number> {
    return this.httpClient.delete<Number>(apiUrl + "/" + voucherId).pipe();
  }

  updateVoucher(voucher: Voucher):Observable<Voucher> {
    return this.httpClient.put<Voucher>(apiUrl, voucher).pipe();
  }
}
