import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Voucher } from '../voucher.component';
import { VoucherService } from '../voucher.service';

@Component({
  selector: 'app-edit-voucher',
  templateUrl: './edit-voucher.component.html',
  styleUrls: ['./edit-voucher.component.css']
})
export class EditVoucherComponent implements OnInit {

  voucher: Voucher = new Voucher();

  constructor(private route: ActivatedRoute, private router: Router, private voucherService: VoucherService) { }

  ngOnInit(): void {
    this.getCurrentVoucher();
  }

  onStartDateChange(event:any) {
    //this.voucher.voucherStartDate = event.target.value;
  }

  onEndDateChange(event:any) {
    //this.voucher.voucherEndDate = event.target.value;
  }

  getCurrentVoucher() {
    const voucherId = Number.parseInt(this.route.snapshot.paramMap.get('voucherId')!);
    this.voucherService.getVoucherById(voucherId).subscribe(voucher => {
      this.voucher = voucher;
    });
  }

  onSubmitEditVoucher(event: any) {
    this.voucherService.updateVoucher(this.voucher).subscribe(res => {
      if (res) alert(`Edit voucher ${this.voucher.voucherCode} successfully !!!`);
      this.router.navigate(['voucher']);
    }, error => {
      alert(`Edit voucher ${this.voucher.voucherCode} failed !! Error: ` + error);
    })
  }

  allowFloat(event:any) {
    if (event.which != 46 && (event.which < 48 || event.which > 57)) {
      event.preventDefault();
      if (event.which != 46 && event.target.value.indexOf('.') != -1) {
        event.preventDefault();
      }
    }
  }
}
